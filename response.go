package splunk

import "time"

// Response represents content of a response from the Splunk API. Splunks own
// explanation of the response elements can be found at https://docs.splunk.com/Documentation/Splunk/latest/RESTUM/RESTusing#Response_elements
type Response struct {
	Origin      string      `json:"origin,omitempty"`
	Updated     time.Time   `json:"updated,omitempty"`
	Generator   Generator   `json:"generator,omitempty"`
	Entry       []Entry     `json:"entry,omitempty"`
	Paging      Paging      `json:"paging,omitempty"`
	Preview     bool        `json:"preview,omitempty"`
	InitOffset  int         `json:"init_offset,omitempty"`
	Messages    []Message   `json:"messages,omitempty"`
	Fields      []Fields    `json:"fields,omitempty"`
	Results     []Results   `json:"results,omitempty"`
	Highlighted Highlighted `json:"highlighted,omitempty"`
	SessionKey  string      `json:"sessionKey,omitempty"`
	Message     string      `json:"message,omitempty"`
	Code        string      `json:"code,omitempty"`
	Sid         string      `json:"sid,omitempty"`
	Links       Links       `json:"links,omitempty"`
}
type Message struct {
	Text string `json:"text,omitempty"`
	Type string `json:"type,omitempty"`
	Code string `json:"code,omitempty"`
}
type Generator struct {
	Build   string `json:"build"`
	Version string `json:"version"`
}
type Links struct {
	Alternate                    string `json:"alternate"`
	SearchTelemetryJSON          string `json:"search_telemetry.json"`
	SearchLog                    string `json:"search.log"`
	Events                       string `json:"events"`
	Results                      string `json:"results"`
	ResultsPreview               string `json:"results_preview"`
	Timeline                     string `json:"timeline"`
	Summary                      string `json:"summary"`
	Control                      string `json:"control"`
	CatalogAllPossiblePredefined string `json:"catalog_allPossible_predefined"`
	Create                       string `json:"create"`
	Reload                       string `json:"_reload"`
	ACL                          string `json:"_acl"`
}
type CommandFields struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
	InputCount   int     `json:"input_count"`
	OutputCount  int     `json:"output_count"`
}
type CommandSearch struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
	InputCount   int     `json:"input_count"`
	OutputCount  int     `json:"output_count"`
}
type CommandSearchCalcfields struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
	InputCount   int     `json:"input_count"`
	OutputCount  int     `json:"output_count"`
}
type CommandSearchExpandSearch struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type CommandSearchExpandSearchCalcfield struct {
	Invocations int `json:"invocations"`
}
type CommandSearchExpandSearchFieldaliaser struct {
	Invocations int `json:"invocations"`
}
type CommandSearchExpandSearchKv struct {
	Invocations int `json:"invocations"`
}
type CommandSearchExpandSearchLookup struct {
	Invocations int `json:"invocations"`
}
type CommandSearchExpandSearchSourcetype struct {
	Invocations int `json:"invocations"`
}
type CommandSearchFieldalias struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
	InputCount   int     `json:"input_count"`
	OutputCount  int     `json:"output_count"`
}
type CommandSearchIndex struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type CommandSearchIndexUsec18 struct {
	Invocations int `json:"invocations"`
}
type CommandSearchIndexUsec409632768 struct {
	Invocations int `json:"invocations"`
}
type CommandSearchIndexUsec5124096 struct {
	Invocations int `json:"invocations"`
}
type CommandSearchIndexUsec64512 struct {
	Invocations int `json:"invocations"`
}
type CommandSearchIndexUsec864 struct {
	Invocations int `json:"invocations"`
}
type CommandSearchKv struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type CommandSearchLookups struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
	InputCount   int     `json:"input_count"`
	OutputCount  int     `json:"output_count"`
}
type CommandSearchParseDirectives struct {
	Invocations int `json:"invocations"`
}
type CommandSearchRawdata struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type CommandSearchSummary struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type CommandSearchTags struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
	InputCount   int     `json:"input_count"`
	OutputCount  int     `json:"output_count"`
}
type CommandSearchTrackSourcetypes struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type CommandSearchTyper struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
	InputCount   int     `json:"input_count"`
	OutputCount  int     `json:"output_count"`
}
type CommandTimeliner struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
	InputCount   int     `json:"input_count"`
	OutputCount  int     `json:"output_count"`
}
type DispatchCreatedSearchResultInfrastructure struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type DispatchEvaluateSearch struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type DispatchFetchRcpPhase0 struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type DispatchFinalWriteToDisk struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type DispatchReadEventsInResults struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type DispatchStreamLocal struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type DispatchTimeline struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type DispatchWriteStatus struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type StartupConfiguration struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type StartupHandoff struct {
	DurationSecs float64 `json:"duration_secs"`
	Invocations  int     `json:"invocations"`
}
type Performance struct {
	CommandFields                             CommandFields                             `json:"command.fields"`
	CommandSearch                             CommandSearch                             `json:"command.search"`
	CommandSearchCalcfields                   CommandSearchCalcfields                   `json:"command.search.calcfields"`
	CommandSearchExpandSearch                 CommandSearchExpandSearch                 `json:"command.search.expand_search"`
	CommandSearchExpandSearchCalcfield        CommandSearchExpandSearchCalcfield        `json:"command.search.expand_search.calcfield"`
	CommandSearchExpandSearchFieldaliaser     CommandSearchExpandSearchFieldaliaser     `json:"command.search.expand_search.fieldaliaser"`
	CommandSearchExpandSearchKv               CommandSearchExpandSearchKv               `json:"command.search.expand_search.kv"`
	CommandSearchExpandSearchLookup           CommandSearchExpandSearchLookup           `json:"command.search.expand_search.lookup"`
	CommandSearchExpandSearchSourcetype       CommandSearchExpandSearchSourcetype       `json:"command.search.expand_search.sourcetype"`
	CommandSearchFieldalias                   CommandSearchFieldalias                   `json:"command.search.fieldalias"`
	CommandSearchIndex                        CommandSearchIndex                        `json:"command.search.index"`
	CommandSearchIndexUsec18                  CommandSearchIndexUsec18                  `json:"command.search.index.usec_1_8"`
	CommandSearchIndexUsec409632768           CommandSearchIndexUsec409632768           `json:"command.search.index.usec_4096_32768"`
	CommandSearchIndexUsec5124096             CommandSearchIndexUsec5124096             `json:"command.search.index.usec_512_4096"`
	CommandSearchIndexUsec64512               CommandSearchIndexUsec64512               `json:"command.search.index.usec_64_512"`
	CommandSearchIndexUsec864                 CommandSearchIndexUsec864                 `json:"command.search.index.usec_8_64"`
	CommandSearchKv                           CommandSearchKv                           `json:"command.search.kv"`
	CommandSearchLookups                      CommandSearchLookups                      `json:"command.search.lookups"`
	CommandSearchParseDirectives              CommandSearchParseDirectives              `json:"command.search.parse_directives"`
	CommandSearchRawdata                      CommandSearchRawdata                      `json:"command.search.rawdata"`
	CommandSearchSummary                      CommandSearchSummary                      `json:"command.search.summary"`
	CommandSearchTags                         CommandSearchTags                         `json:"command.search.tags"`
	CommandSearchTrackSourcetypes             CommandSearchTrackSourcetypes             `json:"command.search.track_sourcetypes"`
	CommandSearchTyper                        CommandSearchTyper                        `json:"command.search.typer"`
	CommandTimeliner                          CommandTimeliner                          `json:"command.timeliner"`
	DispatchCreatedSearchResultInfrastructure DispatchCreatedSearchResultInfrastructure `json:"dispatch.createdSearchResultInfrastructure"`
	DispatchEvaluateSearch                    DispatchEvaluateSearch                    `json:"dispatch.evaluate.search"`
	DispatchFetchRcpPhase0                    DispatchFetchRcpPhase0                    `json:"dispatch.fetch.rcp.phase_0"`
	DispatchFinalWriteToDisk                  DispatchFinalWriteToDisk                  `json:"dispatch.finalWriteToDisk"`
	DispatchReadEventsInResults               DispatchReadEventsInResults               `json:"dispatch.readEventsInResults"`
	DispatchStreamLocal                       DispatchStreamLocal                       `json:"dispatch.stream.local"`
	DispatchTimeline                          DispatchTimeline                          `json:"dispatch.timeline"`
	DispatchWriteStatus                       DispatchWriteStatus                       `json:"dispatch.writeStatus"`
	StartupConfiguration                      StartupConfiguration                      `json:"startup.configuration"`
	StartupHandoff                            StartupHandoff                            `json:"startup.handoff"`
}
type Request struct {
	Search string `json:"search"`
}
type Runtime struct {
	AutoCancel string `json:"auto_cancel"`
	AutoPause  string `json:"auto_pause"`
}
type Content struct {
	CanSummarize                      bool        `json:"canSummarize"`
	CursorTime                        time.Time   `json:"cursorTime"`
	DefaultSaveTTL                    string      `json:"defaultSaveTTL"`
	DefaultTTL                        string      `json:"defaultTTL"`
	Delegate                          string      `json:"delegate"`
	DiskUsage                         int         `json:"diskUsage"`
	DispatchState                     string      `json:"dispatchState"`
	DoneProgress                      int         `json:"doneProgress"`
	DropCount                         int         `json:"dropCount"`
	EarliestTime                      time.Time   `json:"earliestTime"`
	EventAvailableCount               int         `json:"eventAvailableCount"`
	EventCount                        int         `json:"eventCount"`
	EventFieldCount                   int         `json:"eventFieldCount"`
	EventIsStreaming                  bool        `json:"eventIsStreaming"`
	EventIsTruncated                  bool        `json:"eventIsTruncated"`
	EventSearch                       string      `json:"eventSearch"`
	EventSorting                      string      `json:"eventSorting"`
	IndexEarliestTime                 int         `json:"indexEarliestTime"`
	IndexLatestTime                   int         `json:"indexLatestTime"`
	IsBatchModeSearch                 bool        `json:"isBatchModeSearch"`
	IsDone                            bool        `json:"isDone"`
	IsEventsPreviewEnabled            bool        `json:"isEventsPreviewEnabled"`
	IsFailed                          bool        `json:"isFailed"`
	IsFinalized                       bool        `json:"isFinalized"`
	IsPaused                          bool        `json:"isPaused"`
	IsPreviewEnabled                  bool        `json:"isPreviewEnabled"`
	IsRealTimeSearch                  bool        `json:"isRealTimeSearch"`
	IsRemoteTimeline                  bool        `json:"isRemoteTimeline"`
	IsSaved                           bool        `json:"isSaved"`
	IsSavedSearch                     bool        `json:"isSavedSearch"`
	IsTimeCursored                    bool        `json:"isTimeCursored"`
	IsZombie                          bool        `json:"isZombie"`
	Keywords                          string      `json:"keywords"`
	Label                             string      `json:"label"`
	NormalizedSearch                  string      `json:"normalizedSearch"`
	NumPreviews                       int         `json:"numPreviews"`
	OptimizedSearch                   string      `json:"optimizedSearch"`
	Phase0                            string      `json:"phase0"`
	Phase1                            string      `json:"phase1"`
	Pid                               string      `json:"pid"`
	Priority                          int         `json:"priority"`
	Provenance                        string      `json:"provenance"`
	RemoteSearch                      string      `json:"remoteSearch"`
	ReportSearch                      string      `json:"reportSearch"`
	ResultCount                       int         `json:"resultCount"`
	ResultIsStreaming                 bool        `json:"resultIsStreaming"`
	ResultPreviewCount                int         `json:"resultPreviewCount"`
	RunDuration                       float64     `json:"runDuration"`
	SampleRatio                       string      `json:"sampleRatio"`
	SampleSeed                        string      `json:"sampleSeed"`
	ScanCount                         int         `json:"scanCount"`
	Search                            string      `json:"search"`
	SearchCanBeEventType              bool        `json:"searchCanBeEventType"`
	SearchTotalBucketsCount           int         `json:"searchTotalBucketsCount"`
	SearchTotalEliminatedBucketsCount int         `json:"searchTotalEliminatedBucketsCount"`
	Sid                               string      `json:"sid"`
	StatusBuckets                     int         `json:"statusBuckets"`
	TTL                               int         `json:"ttl"`
	WorkloadPool                      string      `json:"workload_pool"`
	Performance                       Performance `json:"performance"`
	Messages                          []Message   `json:"messages"`
	Request                           Request     `json:"request"`
	Runtime                           Runtime     `json:"runtime"`
	SearchProviders                   []string    `json:"searchProviders"`
}
type Perms struct {
	Read  []string `json:"read"`
	Write []string `json:"write"`
}
type ACL struct {
	Perms      Perms  `json:"perms"`
	Owner      string `json:"owner"`
	Modifiable bool   `json:"modifiable"`
	Sharing    string `json:"sharing"`
	App        string `json:"app"`
	CanWrite   bool   `json:"can_write"`
	TTL        string `json:"ttl"`
}
type Entry struct {
	Name      string    `json:"name"`
	ID        string    `json:"id"`
	Updated   time.Time `json:"updated"`
	Links     Links     `json:"links"`
	Published time.Time `json:"published"`
	Author    string    `json:"author"`
	Content   Content   `json:"content"`
	ACL       ACL       `json:"acl"`
}
type Paging struct {
	Total   int `json:"total"`
	PerPage int `json:"perPage"`
	Offset  int `json:"offset"`
}
type Fields struct {
	Name string `json:"name"`
}
type Results struct {
	_Bkt         string    `json:"_bkt"`
	_Cd          string    `json:"_cd"`
	_Indextime   string    `json:"_indextime"`
	_Raw         string    `json:"_raw"`
	_Serial      string    `json:"_serial"`
	_Si          []string  `json:"_si"`
	_Sourcetype  string    `json:"_sourcetype"`
	Subsecond    string    `json:"_subsecond"`
	Time         time.Time `json:"_time"`
	Host         string    `json:"host"`
	Index        string    `json:"index"`
	Linecount    string    `json:"linecount"`
	Source       string    `json:"source"`
	Sourcetype   string    `json:"sourcetype"`
	SplunkServer string    `json:"splunk_server"`
}
type Highlighted struct {
}
