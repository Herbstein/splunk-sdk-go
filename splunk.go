// Package Splunk is an unofficial SDK for Splunk in Go. It attempts to stay as
// close to Splunks own API documentation, so as to not cause confusion when
// reading Splunks own documentation.
//
// Links for Splunk Enterprise API documentation:
// https://docs.splunk.com/Documentation/Splunk/latest/RESTUM/RESTusing
// https://docs.splunk.com/Documentation/Splunk/8.0.4/RESTREF/RESTprolog

package splunk

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"reflect"
	"strings"
)

var (
	// ErrRequiredField is used when fields that are required are either empty or
	// non-existent.
	ErrRequiredField      = errors.New("field is required")
	ErrUnknown            = errors.New("unknown error")
	ErrInvalidCredentials = errors.New("invalid credentials")
	ErrInvalidNamespace   = errors.New("invalid namespace")
	// ErrUnexpected is used whenever an unexpected input is given or an
	// unexpected output is provided. This includes whenever the Splunk Enterprise
	// API returns empty HTTP bodies (When it's supposed to provide a body) or
	// when the API returns the wrong content-type. Generally, these errors should
	// be rare.
	ErrUnexpected = errors.New("unexpected input/output")
)

var (
	MessageCodeError = "ERROR"
)

// Http status codes used by the Splunk Enterprise API, as per
// https://docs.splunk.com/Documentation/Splunk/latest/RESTUM/RESTusing#HTTP_Status_Codes
const (
	HttpStatusOK                  = http.StatusOK                  //	Request completed successfully.
	HttpStatusCreated             = http.StatusCreated             // Create request completed successfully.
	HttpStatusBadRequest          = http.StatusBadRequest          // Request error. See response body for details.
	HttpStatusUnauthorized        = http.StatusUnauthorized        // Authentication failure, invalid access credentials.
	HttpStatusPaymentRequired     = http.StatusPaymentRequired     // In-use Splunk Enterprise license disables this feature.
	HttpStatusForbidden           = http.StatusForbidden           // Insufficient permission.
	HttpStatusNotFound            = http.StatusNotFound            // Requested endpoint does not exist.
	HttpStatusConflict            = http.StatusConflict            // Invalid operation for this endpoint. See response body for details.
	HttpStatusInternalServerError = http.StatusInternalServerError // Unspecified internal server error. See response body for details.
	HttpStatusServiceUnavailable  = http.StatusServiceUnavailable  // Feature is disabled in configuration file.
)

// Namespace is the container used to limit an action to a namespace. A
// namespace consists of a user and an app. This is also commonly referred to as
// a context inside of Splunk.
type Namespace struct {
	User string
	App  string
}

// NewNamespace creates a new namespace for the user 'nobody' and the app
// 'search'.
func NewNamespace() Namespace {
	return Namespace{
		User: "nobody",
		App:  "search",
	}
}

// nsNoWildcard checks that namespace isn't set to wildcards.
func nsNoWildcard(ns Namespace) error {
	if ns.User == "-" || ns.App == "-" {
		return wrapError("wildcards (-) not allowed in namespace", ErrInvalidNamespace)
	}
	return nil
}

// nsEnsureNamespace checks that a namespace is set.
func nsEnsureNamespace(ns Namespace) error {
	if ns.User == "" || ns.App == "" {
		return wrapError("namespace must contain user and app", ErrInvalidNamespace)
	}
	return nil
}

// HttpClient is the interface for interacting with the outside world using an
// HTTP client. An interface is chosen, instead of the http.Client type (Which
// implements this interface) as to allow mock implementations for testing.
type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// Connection is the object with which one interacts with the Splunk Enterprise
// API.
type Connection struct {
	Username   string `splunk:"username,required"`
	Password   string `splunk:"password,required"`
	BaseURL    string
	sessionKey string
	http       HttpClient
	httpHeader http.Header
}

// NewConnection creates a new instance of Connection.
// If httpClient is nil, a default http.Client will be created and used for
// http calls.
func NewConnection(username string, password string, baseurl string, client HttpClient) *Connection {
	if client == nil {
		client = &http.Client{}
	}
	return &Connection{
		Username: username,
		Password: password,
		BaseURL:  baseurl,
		http:     client,
	}
}

// wrapError wraps an error using the wrapping technique from Go 1.13. We're
// wrapping as to avoid making errors, defined outside of this package, part of
// this packages API. If a non-empty string is provided as 's', it will be
// prepended to the provided 'err'.
func wrapError(s string, err error) error {
	if s == "" {
		return fmt.Errorf("%w", err)
	}
	return fmt.Errorf("%s: %w", s, err)
}

// httpGet is a helper function that creates a HTTP Get request. The provided
// data is 'url encoded' and appended to 'resource' as query parameters.
func (c *Connection) httpGet(resource string, data *url.Values) (*http.Response, error) {
	if data != nil {
		oUrl, err := url.Parse(resource)
		query := oUrl.Query()
		for k, va := range *data {
			for _, v := range va {
				if query.Get(k) == "" {
					query.Set(k, v)
				} else {
					query.Add(k, v)
				}
			}
		}

		oUrl.RawQuery = query.Encode()
		resource = oUrl.String()
		if err != nil {
			return nil, fmt.Errorf("unable to add url parameters: %v", err)
		}
	}
	return c.httpRequest(resource, http.MethodGet, nil)
}

// httpPost is a helper function that creates a HTTP Post request.
func (c *Connection) httpPost(resource string, data *url.Values) (*http.Response, error) {
	return c.httpRequest(resource, http.MethodPost, data)
}

// httpDelete is a helper function that creates a HTTP Post request.
func (c *Connection) httpDelete(resource string) (*http.Response, error) {
	return c.httpRequest(resource, http.MethodDelete, nil)
}

// httpRequest is a helper function that creates a HTTP request.
func (c *Connection) httpRequest(resource string, method string, data *url.Values) (*http.Response, error) {
	var payload io.Reader
	if data != nil {
		payload = bytes.NewBufferString(data.Encode())
	}

	request, err := http.NewRequest(method, resource, payload)
	if err != nil {
		return nil, fmt.Errorf("cannot create request: %v", err)
	}

	if c.httpHeader != nil {
		request.Header = c.httpHeader
	}

	if c.sessionKey != "" {
		request.Header.Add("Authorization", fmt.Sprintf("Splunk %s", c.sessionKey))
	} else if c.Username != "" && c.Password != "" {
		request.SetBasicAuth(c.Username, c.Password)
	}

	response, err := c.http.Do(request)

	if err != nil {
		return nil, fmt.Errorf("request error: %v", err)
	}
	return response, nil
}

// Login will attempt to login to the Splunk Enterprise API pointed to in
// 'BaseURL'. Credentials from 'Username' and 'Password' will be used. If
// successful, a nil error will be returned.
func (c *Connection) Login() error {
	if c.BaseURL == "" {
		return wrapError("BaseURL is missing", ErrRequiredField)
	}
	d, err := populateValues(c)
	if err != nil {
		return wrapError("", err)
	}

	d.Add("output_mode", "json")
	r, err := c.httpPost(fmt.Sprintf("%s/services/auth/login", c.BaseURL), d)

	if err != nil {
		return wrapError("", err)
	}
	defer r.Body.Close()

	if r.StatusCode != http.StatusOK {
		if r.StatusCode == http.StatusUnauthorized {
			return ErrInvalidCredentials
		} else {
			return wrapError(fmt.Sprintf("Splunk API response: %d", r.StatusCode), ErrUnknown)
		}
	}

	sessionKey := Response{}
	err = json.NewDecoder(r.Body).Decode(&sessionKey)
	if err != nil {
		return fmt.Errorf("%v", err)
	}
	c.sessionKey = sessionKey.SessionKey
	return nil
}

type splunkTag struct {
	Name      string
	Omitempty bool
	Required  bool
}

func parseSplunkTag(tag string) splunkTag {
	a := strings.Split(tag, ",")
	t := splunkTag{
		Name: a[0],
	}
	for _, v := range a {
		switch v {
		case "omitempty":
			t.Omitempty = true
		case "required":
			t.Required = true
		}
	}
	return t
}

func populateValues(construct interface{}) (*url.Values, error) {
	payload := url.Values{}
	rv := reflect.ValueOf(construct)
	t := rv.Type()
	if t.Kind() == reflect.Ptr {
		rv = reflect.Indirect(reflect.ValueOf(construct))
		t = rv.Type()
	}
	for i := 0; i < t.NumField(); i++ {
		value, ok := t.Field(i).Tag.Lookup("splunk")
		if ok {
			switch rv.Field(i).Kind() {
			case reflect.String:
				rvValue := rv.Field(i).String()
				sTag := parseSplunkTag(value)
				if sTag.Required && rvValue == "" {
					return nil, wrapError(sTag.Name, ErrRequiredField)
				}
				if rvValue == "" && sTag.Omitempty {
					continue
				}
				payload.Add(sTag.Name, rvValue)
			case reflect.Ptr:
				pv := reflect.Indirect(rv.Field(i))
				switch pv.Kind() {
				case reflect.Bool:
					pvValue := pv.Bool()
					sTag := parseSplunkTag(value)
					payload.Add(sTag.Name, fmt.Sprintf("%t", pvValue))
				case reflect.Int:
					pvValue := pv.Int()
					sTag := parseSplunkTag(value)
					payload.Add(sTag.Name, fmt.Sprintf("%d", pvValue))
				}
			}
		}
	}
	return &payload, nil
}
