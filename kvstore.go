package splunk

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Collection struct {
	Name                 string `splunk:"name,required" json:"name"`
	ProfilingEnabled     *bool  `splunk:"profilingEnabled,omitempty" json:"profilingEnabled"`
	ProfilingThresholdMs *int   `splunk:"profilingThresholdMs,omitempty" json:"profilingThresholdMs"`
	Disabled             *bool  `splunk:"disabled,omitempty" json:"disabled"`
	Namespace            Namespace
}

// CreateCollection attempts to create the collection 'col'.
// Splunks own documentation for creating collections can be found at
// https://docs.splunk.com/Documentation/Splunk/8.0.4/RESTREF/RESTkvstore
func (c *Connection) CreateCollection(col *Collection) (*Response, error) {

	err := nsNoWildcard(col.Namespace)
	if err != nil {
		return nil, wrapError("", err)
	}
	err = nsEnsureNamespace(col.Namespace)
	if err != nil {
		return nil, wrapError("", err)
	}

	d, err := populateValues(c)
	if err != nil {
		return nil, wrapError("", err)
	}
	d.Add("output_mode", "json")
	r, err := c.httpGet(fmt.Sprintf("%s/servicesNs/%s/%s/storage/collections/config", c.BaseURL, col.Namespace.User, col.Namespace.App), d)
	if err != nil {
		return nil, err
	}

	if r.Body == nil {
		return nil, wrapError("splunk did not return a body", ErrUnexpected)
	}
	if r.Header.Get("content-type") != "application/json" {
		return nil, wrapError("wrong content-type", ErrUnexpected)

	}
	splunkResp := Response{}
	err = json.NewDecoder(r.Body).Decode(&splunkResp)
	if err != nil {
		return nil, fmt.Errorf("%v", err)
	}
	if splunkResp.Messages != nil {
		for _, m := range splunkResp.Messages {
			if m.Type == MessageCodeError {
				hasUser := strings.Contains(m.Text, "Cannot get username")
				hasApp := strings.Contains(m.Text, "Cannot get app")
				if hasUser == false && hasApp == false {
					return nil, wrapError("username and app cannot be wildcard", ErrInvalidNamespace)
				} else if hasUser == false {
					return nil, wrapError("username cannot be wildcard", ErrInvalidNamespace)
				} else if hasApp == false {
					return nil, wrapError("app cannot be wildcard", ErrInvalidNamespace)
				} else {
					return nil, wrapError(fmt.Sprintf("%d: %s", r.StatusCode, m.Text), ErrUnknown)
				}
			}
		}
	}
	defer r.Body.Close()
	return &splunkResp, nil
}
