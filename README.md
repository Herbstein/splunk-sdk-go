# An SDK for Splunk in Go

[![coverage report](https://gitlab.com/MadsRC/splunk-sdk-go/badges/master/coverage.svg)](https://gitlab.com/MadsRC/splunk-sdk-go/-/commits/master)
[![pipeline status](https://gitlab.com/MadsRC/splunk-sdk-go/badges/master/pipeline.svg)](https://gitlab.com/MadsRC/splunk-sdk-go/-/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/MadsRC/splunk-sdk-go?status.svg)](http://godoc.org/gitlab.com/MadsRC/splunk-sdk-go)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/MadsRC/splunk-sdk-go)](https://goreportcard.com/report/gitlab.com/MadsRC/splunk-sdk-go)

This is a development kit for interacting with the Splunk Enterprise API
from the Go programming language.

For information and guidance on using the package, refer to the Go package
documentation at
[this URL](https://pkg.go.dev/gitlab.com/MadsRC/splunk-sdk-go).

## Version disclaimer

As the project has not reached version 1.0.0, and per
[SemVer v2.0.0 Point 4](https://semver.org/spec/v2.0.0.html#spec-item-4), one
should not consider the public API stable.

## Project conventions

To make maintaining this project easier, the following conventions has been
selected and will be applied to the development and release cycle of this
project.

1. [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html)
1. [Conventional Commits 1.0.0](https://www.conventionalcommits.org/en/v1.0.0/)
1. [KeepAChangelog 1.0.0](https://keepachangelog.com/en/1.0.0/)
