package mock

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

// DoLoginOKReturn is a mocked HTTP response from the Splunk Enterprise API
// with a JSON body. It is an example of the returned response when logging in
// successfully.
func DoLoginOKReturn(req *http.Request) (*http.Response, error) {
	body := `{"sessionKey":"IYTuT24wCZOxkgIbES1qpNC9ZLaK5hrvDshUnLOb9lqGmSdmSx6c2DoqG7ZxHlu0gsaCaI4NaAw7i6^KMH00ivXArh0Tvatd6sx1KC","message":"","code":""}`
	header := make(http.Header)
	header.Set("Content-Type", "application/json")
	header.Add("Content-Type", "charset=UTF-8")
	header.Set("X-Content-Type-Options", "nosniff")
	header.Set("Connection", "Keep-Alive")
	header.Set("X-Frame-Options", "SAMEORIGIN")
	header.Set("Server", "Splunkd")
	t := &http.Response{
		Status:        "200 OK",
		StatusCode:    200,
		Proto:         "HTTP/1.1",
		ProtoMajor:    1,
		ProtoMinor:    1,
		Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
		ContentLength: int64(len(body)),
		Header:        header,
	}
	return t, nil
}

// DoLoginWrongCredReturn is a mocked HTTP response from the Splunk Enterprise
// API with a JSON body. It is an example of the returned response when
// attempting to login without the correct credentials.
func DoLoginWrongCredReturn(req *http.Request) (*http.Response, error) {
	body := `{"messages":[{"type":"WARN","code":"incorrect_username_or_password","text":"Login failed"}]}`
	header := make(http.Header)
	header.Set("Content-Type", "application/json")
	header.Add("Content-Type", "charset=UTF-8")
	header.Set("X-Content-Type-Options", "nosniff")
	header.Set("Connection", "Keep-Alive")
	header.Set("X-Frame-Options", "SAMEORIGIN")
	header.Set("Server", "Splunkd")
	t := &http.Response{
		Status:        "401 Unauthorized",
		StatusCode:    401,
		Proto:         "HTTP/1.1",
		ProtoMajor:    1,
		ProtoMinor:    1,
		Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
		ContentLength: int64(len(body)),
		Header:        header,
	}
	return t, nil
}
