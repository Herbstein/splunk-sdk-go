package mock

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

// DoCreateCollectionOKReturn is a mocked HTTP response from the Splunk
// Enterprise API, with a JSON body. It is an example of a response given when
// creating a new KV collection.
func DoCreateCollectionOKReturn(req *http.Request) (*http.Response, error) {
	body := `{"links":{"create":"/servicesNS/nobody/search/storage/collections/config/_new","_reload":"/servicesNS/nobody/search/storage/collections/config/_reload","_acl":"/servicesNS/nobody/search/storage/collections/config/_acl"},"origin":"https://localhost:8089/servicesNS/nobody/search/storage/collections/config","updated":"2020-06-23T19:17:05+00:00","generator":{"build":"767223ac207f","version":"8.0.4"},"entry":[{"name":"test1","id":"https://localhost:8089/servicesNS/nobody/search/storage/collections/config/test1","updated":"2020-06-23T19:17:05+00:00","links":{"alternate":"/servicesNS/nobody/search/storage/collections/config/test1","list":"/servicesNS/nobody/search/storage/collections/config/test1","_reload":"/servicesNS/nobody/search/storage/collections/config/test1/_reload","edit":"/servicesNS/nobody/search/storage/collections/config/test1","remove":"/servicesNS/nobody/search/storage/collections/config/test1","disable":"/servicesNS/nobody/search/storage/collections/config/test1/disable"},"author":"admin","acl":{"app":"search","can_change_perms":true,"can_list":true,"can_share_app":true,"can_share_global":true,"can_share_user":true,"can_write":true,"modifiable":true,"owner":"admin","perms":{"read":["*"],"write":["admin","power"]},"removable":true,"sharing":"app"},"content":{"disabled":false,"eai:acl":null,"eai:appName":"search","eai:userName":"nobody","profilingEnabled":"false","profilingThresholdMs":"1000","replicate":"false","replication_dump_maximum_file_size":"10240","replication_dump_strategy":"auto","type":"undefined"}}],"paging":{"total":1,"perPage":30,"offset":0},"messages":[]}`
	header := make(http.Header)
	header.Set("Content-Type", "application/json")
	header.Add("Content-Type", "charset=UTF-8")
	header.Set("X-Content-Type-Options", "nosniff")
	header.Set("Connection", "Keep-Alive")
	header.Set("X-Frame-Options", "SAMEORIGIN")
	header.Set("Server", "Splunkd")
	t := &http.Response{
		Status:        "201 Created",
		StatusCode:    201,
		Proto:         "HTTP/1.1",
		ProtoMajor:    1,
		ProtoMinor:    1,
		Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
		ContentLength: int64(len(body)),
		Header:        header,
	}
	return t, nil
}

// DoCreateCollectionErrAllUserReturn is a mocked HTTP response from the Splunk
// Enterprise API, with a JSON body. It is an example of a response given when
// attempting to create a KV collection with a wildcard in the user section
// of the namespace.
func DoCreateCollectionErrAllUserReturn(req *http.Request) (*http.Response, error) {
	body := `{"messages":[{"type":"ERROR","text":"Cannot get username when all users are selected."}]}`
	header := make(http.Header)
	header.Set("Content-Type", "application/json")
	header.Add("Content-Type", "charset=UTF-8")
	header.Set("X-Content-Type-Options", "nosniff")
	header.Set("Connection", "Keep-Alive")
	header.Set("X-Frame-Options", "SAMEORIGIN")
	header.Set("Server", "Splunkd")
	t := &http.Response{
		Status:        "500 Internal Server Error",
		StatusCode:    500,
		Proto:         "HTTP/1.1",
		ProtoMajor:    1,
		ProtoMinor:    1,
		Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
		ContentLength: int64(len(body)),
		Header:        header,
	}
	return t, nil
}

// DoCreateCollectionErrAllUserReturn is a mocked HTTP response from the Splunk
// Enterprise API, with a JSON body. It is an example of a response given when
// attempting to create a KV collection with a wildcard in the app section
// of the namespace.
func DoCreateCollectionErrAllAppReturn(req *http.Request) (*http.Response, error) {
	body := `{"messages":[{"type":"ERROR","text":"Cannot get app name when all apps are selected."}]}`
	header := make(http.Header)
	header.Set("Content-Type", "application/json")
	header.Add("Content-Type", "charset=UTF-8")
	header.Set("X-Content-Type-Options", "nosniff")
	header.Set("Connection", "Keep-Alive")
	header.Set("X-Frame-Options", "SAMEORIGIN")
	header.Set("Server", "Splunkd")
	t := &http.Response{
		Status:        "500 Internal Server Error",
		StatusCode:    500,
		Proto:         "HTTP/1.1",
		ProtoMajor:    1,
		ProtoMinor:    1,
		Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
		ContentLength: int64(len(body)),
		Header:        header,
	}
	return t, nil
}
