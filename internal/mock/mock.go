package mock

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

type MockHttpClient struct {
	DoFunc    func(req *http.Request) (*http.Response, error)
	DoInvoked bool
}

func (m *MockHttpClient) Do(req *http.Request) (*http.Response, error) {
	m.DoInvoked = true
	return m.DoFunc(req)
}

func DoNilReturn(req *http.Request) (*http.Response, error) {
	return nil, nil
}

func DoContentTypeURLEncodedReturn(req *http.Request) (*http.Response, error) {
	body := ``
	header := make(http.Header)
	header.Set("Content-Type", "application/x-www-form-urlencoded")
	header.Add("Content-Type", "charset=UTF-8")
	header.Set("X-Content-Type-Options", "nosniff")
	header.Set("Connection", "Keep-Alive")
	header.Set("X-Frame-Options", "SAMEORIGIN")
	header.Set("Server", "Splunkd")
	t := &http.Response{
		Status:        "200 OK",
		StatusCode:    200,
		Proto:         "HTTP/1.1",
		ProtoMajor:    1,
		ProtoMinor:    1,
		Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
		ContentLength: int64(len(body)),
		Header:        header,
	}
	return t, nil
}

func DoNilBodyReturn(req *http.Request) (*http.Response, error) {
	header := make(http.Header)
	header.Add("Content-Type", "charset=UTF-8")
	header.Set("X-Content-Type-Options", "nosniff")
	header.Set("Connection", "Keep-Alive")
	header.Set("X-Frame-Options", "SAMEORIGIN")
	header.Set("Server", "Splunkd")
	t := &http.Response{
		Status:     "200 OK",
		StatusCode: 200,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 1,
		Header:     header,
	}
	return t, nil
}
