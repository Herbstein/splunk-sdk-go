# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1] - 26/06/2020
### Added
 - The initial release of this package

[Unreleased]: https://gitlab.com/MadsRC/splunk-sdk-go/-/compare/v0.0.1...master
[0.0.1]: https://gitlab.com/MadsRC/splunk-sdk-go/-/releases/v0.0.1
