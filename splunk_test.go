package splunk

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/MadsRC/splunk-sdk-go/internal/mock"
)

func TestConnection(t *testing.T) {
	t.Run("Test NewConnection", func(t *testing.T) {
		client := mock.MockHttpClient{}
		client.DoFunc = mock.DoNilReturn
		username := "admin"
		password := "thisisstupid1234"
		baseurl := "https://localhost:8089"
		conn := NewConnection(username, password, baseurl, &client)
		if conn.Username != username {
			t.Errorf("Expected '%s', got: '%s'", username, conn.Username)
		}
		if conn.Password != password {
			t.Errorf("Expected '%s', got: '%s'", password, conn.Password)
		}
		if conn.BaseURL != baseurl {
			t.Errorf("Expected '%s', got: '%s'", baseurl, conn.BaseURL)
		}
	})

	t.Run("Test Login", func(t *testing.T) {
		testTables := []struct {
			username string
			password string
			baseurl  string
			err      error
			mock     func(req *http.Request) (*http.Response, error)
		}{
			{"admin", "thisisstupid", "https://localhost:8089", nil, mock.DoLoginOKReturn},
			{"admin", "", "https://localhost:8089", ErrRequiredField, mock.DoLoginOKReturn},
			{"", "thisisstupid", "https://localhost:8089", ErrRequiredField, mock.DoLoginOKReturn},
			{"admin", "thisisstupid", "", ErrRequiredField, mock.DoLoginOKReturn},
			{"admin", "verywrongpass", "https://localhost:8089", ErrInvalidCredentials, mock.DoLoginWrongCredReturn},
		}
		for i, testTable := range testTables {
			t.Run(fmt.Sprintf("[%d]", i), func(t *testing.T) {
				client := mock.MockHttpClient{}
				client.DoFunc = testTable.mock
				conn := NewConnection(testTable.username, testTable.password, testTable.baseurl, &client)
				err := conn.Login()
				if err != nil && testTable.err == nil {
					t.Errorf("Unexpected error: %+v", err)
				} else if !errors.Is(err, testTable.err) {
					t.Errorf("Expected err to be '%+v', got '%+v'", testTable.err, err)
				}
				if testTable.err == nil {
					if conn.sessionKey == "" {
						t.Errorf("Expected connection to have a session key assigned. Had an empty string")
					}
				}
			})
		}
	})
}

func TestPopulateValues(t *testing.T) {
	type TestStruct struct {
		RequiredField    string `splunk:"required_field,required" json:"requiredField"`
		OptionalField    string `splunk:"optional_field,omitempty"`
		AlwaysThereField string `splunk:"always_there_field"`
		AnInt            *int   `splunk:"an_int,omitempty"`
		ABool            *bool  `splunk:"a_bool,omitempty"`
		ARequiredBool    *bool  `splunk:"a_required_bool,required"`
		ARequiredInt     *int   `splunk:"a_required_int,required"`
		IntHere          *int   `splunk:"int_here,omitempty"`
		BoolHere         *bool  `splunk:"bool_here,omitempty"`
	}
	falseBool := false
	zeroInt := 0
	anInt := 10
	trueBool := true

	t.Run("Proper parsing", func(t *testing.T) {
		s := TestStruct{
			RequiredField:    "I have to be here",
			OptionalField:    "I don't have to be here",
			AlwaysThereField: "I'm THAT guy...",
			ARequiredBool:    &falseBool,
			ARequiredInt:     &zeroInt,
			IntHere:          &anInt,
			BoolHere:         &trueBool,
		}
		expectedPayload := url.Values{
			"required_field":     []string{s.RequiredField},
			"optional_field":     []string{s.OptionalField},
			"always_there_field": []string{s.AlwaysThereField},
			"a_required_bool":    []string{fmt.Sprintf("%t", *s.ARequiredBool)},
			"a_required_int":     []string{fmt.Sprintf("%d", *s.ARequiredInt)},
			"int_here":           []string{fmt.Sprintf("%d", *s.IntHere)},
			"bool_here":          []string{fmt.Sprintf("%t", *s.BoolHere)},
		}
		p, err := populateValues(s)
		if err != nil {
			t.Errorf("Expected nil err, got: %+v\n", err)
		}
		if p.Encode() != expectedPayload.Encode() {
			t.Errorf("Expected '%+v', got '%+v'", expectedPayload, p)
		}
	})

	t.Run("Required field missing", func(t *testing.T) {
		s := TestStruct{
			OptionalField:    "I don't have to be here",
			AlwaysThereField: "I'm THAT guy...",
			ARequiredBool:    &falseBool,
			ARequiredInt:     &zeroInt,
		}
		p, err := populateValues(s)
		if !errors.Is(err, ErrRequiredField) {
			t.Errorf("Expected ErrRequiredField, got %+v", err)
		}
		if p != nil {
			t.Errorf("Expected nil return, got: %+v", p)
		}
	})

	t.Run("Omitempty", func(t *testing.T) {
		s := TestStruct{
			RequiredField:    "I have to be here",
			AlwaysThereField: "I'm THAT guy...",
			ARequiredBool:    &falseBool,
			ARequiredInt:     &zeroInt,
		}
		expectedPayload := url.Values{
			"required_field":     []string{s.RequiredField},
			"always_there_field": []string{s.AlwaysThereField},
			"a_required_bool":    []string{fmt.Sprintf("%t", *s.ARequiredBool)},
			"a_required_int":     []string{fmt.Sprintf("%d", *s.ARequiredInt)},
		}
		p, err := populateValues(s)
		if err != nil {
			t.Errorf("Expected nil err, got: %+v\n", err)
		}
		if p.Encode() != expectedPayload.Encode() {
			t.Errorf("Expected '%+v', got '%+v'", expectedPayload, p)
		}
	})
}
