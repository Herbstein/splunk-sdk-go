package splunk

import (
	"errors"
	"fmt"
	"net/http"
	"testing"

	"gitlab.com/MadsRC/splunk-sdk-go/internal/mock"
)

func TestCollection(t *testing.T) {
	col1 := Collection{
		Name: "col1",
		Namespace: Namespace{
			User: "nobody",
			App:  "search",
		},
	}

	t.Run("CreateCollection", func(t *testing.T) {
		testTables := []struct {
			err  error
			mock func(req *http.Request) (*http.Response, error)
		}{
			{nil, mock.DoCreateCollectionOKReturn},
			{ErrInvalidNamespace, mock.DoCreateCollectionErrAllUserReturn},
			{ErrInvalidNamespace, mock.DoCreateCollectionErrAllAppReturn},
			{ErrUnexpected, mock.DoNilBodyReturn},
			{ErrUnexpected, mock.DoContentTypeURLEncodedReturn},
		}
		for i, testTable := range testTables {
			t.Run(fmt.Sprintf("[%d]", i), func(t *testing.T) {
				client := mock.MockHttpClient{}
				client.DoFunc = testTable.mock
				conn := NewConnection("admin", "thisisstupid1234", "https://127.0.0.1:8089", &client)
				_, err := conn.CreateCollection(&col1)
				if err != nil && testTable.err == nil {
					t.Errorf("Unexpected error: %+v", err)
				} else if !errors.Is(err, testTable.err) {
					t.Errorf("Expected err to be '%+v', got '%+v'", testTable.err, err)
				}
			})
		}
	})
}
